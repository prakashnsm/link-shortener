# link-shortener

[![License](http://img.shields.io/:license-apache-blue.svg)](http://www.apache.org/licenses/LICENSE-2.0.html)

This is a small demo application for url link shorten on [Heroku](http://heroku.com).
You can test the application [here](https://linkshrtner.herokuapp.com/). Note that it is only running on a
[free dyno](https://www.heroku.com/pricing), so it may take some time before it responds.

[![Deploy to Heroku](https://www.herokucdn.com/deploy/button.png)](https://heroku.com/deploy)

## Running the application

To run the application, run this `com.hubapps.demo.Application` java class as
a Java Application.
Alternatively the application can be started from the terminal using maven with `mvn spring-boot:run`.
After starting the application, go to your browser to http://localhost:4555.

## Default 

For running the application using H2 embedded database.

## Using a Postgres database for persistence

For running the application using a real [Postgres](http://www.postgresql.org/) database, uncomment all property
definitions in the `application.properties` file and put the configuration for your Postgres instance there.

## License

Code is under the [Apache Licence v2](https://www.apache.org/licenses/LICENSE-2.0.txt).
