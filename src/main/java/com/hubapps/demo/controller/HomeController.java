/*
 * Copyright 2017 Prakash Vaithilingam
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hubapps.demo.controller;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.UrlValidator;
import org.hibernate.mapping.Array;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.common.hash.Hashing;
import com.hubapps.demo.model.Activity;
import com.hubapps.demo.model.Url;
import com.hubapps.demo.repository.ActivityRepository;
import com.hubapps.demo.repository.UrlRepository;

@Controller
@RequestMapping("/")
public class HomeController {

	@Autowired
    private UrlRepository urlRepository;
	
	@Autowired
    private ActivityRepository activityRepository;

	@RequestMapping(method = RequestMethod.GET)
	public String home(ModelMap model) {
		List<Url> urls = urlRepository.findAll();
        Collections.sort(urls, new Comparator<Url>() {
			@Override
			public int compare(Url o1, Url o2) {
				//ascending order
				//return new Long(o1.getId()).compareTo(o2.getId());
				//descending order
			    return new Long(o2.getId()).compareTo(o1.getId());
			}
		});
		model.addAttribute("urls", urls);
		model.addAttribute("saveUrl", new Url());
		return "home";
	}
	
    @RequestMapping(value = "/{key}", method = RequestMethod.GET)
    public void redirect(@PathVariable String key, HttpServletRequest req, HttpServletResponse resp) throws Exception {
        Url u = urlRepository.findByKey(key);
        if (u != null && !StringUtils.isEmpty(u.getUrl())){
        	Activity a = new Activity();
        	a.setAccessDate(new Date());
        	a.setClientIp(req.getRemoteHost());
        	a.setUrlKey(req.getRemoteAddr());
        	u.getActivites().add(a);
        	a.setUrl(u);
        	activityRepository.saveAndFlush(a);
        	
        	resp.sendRedirect(u.getUrl());
        }
        else {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
        }
    }
    
    @RequestMapping(value = "/details/{key}", method = RequestMethod.GET)
    public String details(@PathVariable String key, HttpServletRequest req, ModelMap model) throws Exception {
        Url u = urlRepository.findByKey(key);
        ArrayList<Activity> list = new ArrayList<Activity>(u.getActivites());
        Collections.sort(list, new Comparator<Activity>() {
			@Override
			public int compare(Activity o1, Activity o2) {
				//ascending order
				//return new Long(o1.getId()).compareTo(o2.getId());
				//descending order
			    return new Long(o2.getId()).compareTo(o1.getId());
			}
		});
        model.addAttribute("activities", list);
        return "details";
    }
    
	@RequestMapping(method = RequestMethod.POST)
    public String save(ModelMap model, 
            @ModelAttribute("saveUrl") @Valid Url record,
            BindingResult result) {
        final String url = record.getUrl();
        final UrlValidator urlValidator = new UrlValidator(new String[]{"http", "https"});
        if (urlValidator.isValid(url)) {
            final String key = Hashing.murmur3_32().hashString(url, StandardCharsets.UTF_8).toString();
            Url u = urlRepository.findByKey(key);
            if(u == null){
            	u = new Url();
            	u.setKey(key);
            	u.setUrl(url);
            	urlRepository.saveAndFlush(u);
            }
        } 
        return home(model);
    }
	
	public void setUrlRepository(UrlRepository urlRepository) {
		this.urlRepository = urlRepository;
	}
    
   
}
