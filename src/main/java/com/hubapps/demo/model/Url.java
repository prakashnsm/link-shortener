package com.hubapps.demo.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Url {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    @Column( unique = true, nullable = false)
    private String key;
    
    @Column(length=2500, nullable = false)
    private String url;
    
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "url")
    private Set<Activity> activites = new HashSet<Activity>(0);

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Set<Activity> getActivites() {
		return activites;
	}

	public void setActivites(Set<Activity> activites) {
		this.activites = activites;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null || ! (obj instanceof Url)) return false;
		
		Url u  = (Url) obj;
		if((u.getId() >0 && this.id > 0 && this.id == u.getId())
				|| (u.getKey() != null && this.key != null && this.key.equals(u.getKey()))
					|| (u.getUrl() != null && this.url != null && this.url.equals(u.getUrl()))){
			return true;
		}
		return false;
	}

	@Override
	public int hashCode() {
		return (this.url != null ? this.url.hashCode() : 0 ) + (this.key != null ? this.key.hashCode() : 0 );
	}
}
