package com.hubapps.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hubapps.demo.model.Url;

@Repository
public interface UrlRepository extends JpaRepository<Url, Long> {
	
	Url findByKey(String key);
	
	Url findByUrl(String url);
}
