package com.hubapps.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hubapps.demo.model.Activity;

@Repository
public interface ActivityRepository extends JpaRepository<Activity, Long> {
	
}
