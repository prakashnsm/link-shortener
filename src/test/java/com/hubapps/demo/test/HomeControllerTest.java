package com.hubapps.demo.test;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.ui.ModelMap;
import org.springframework.web.context.WebApplicationContext;

import com.hubapps.demo.Application;
import com.hubapps.demo.controller.HomeController;
import com.hubapps.demo.model.Url;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = Application.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class HomeControllerTest {

	private MockMvc mockMvc;
	
	@Autowired
    private WebApplicationContext wac;
	
	@Autowired
	private HomeController homeController;

	@Autowired 
	protected HttpServletRequest httpRequest;
	@Autowired 
	protected HttpServletResponse httpResponse;
	@Before
	public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();

	}

	@Test
	public void verifyUrlSaveAndList() throws Exception {
		ModelMap model = new ModelMap();
		
		// home view
		homeController.home(model);
		Assert.assertTrue(model.containsKey("urls"));
		Assert.assertNotNull(model.get("urls"));
		
		
		// url shorten generate, save and retrieve list all values
		String urlStr = "https://bitbucket.org/prakashnsm/link-shortener/src";
		Url url = new Url();
		url.setUrl(urlStr);
		String view = homeController.save(model, url, null);
		Assert.assertTrue(model.containsKey("urls"));
		List<Url> urls = (List<Url>) model.get("urls");
		Assert.assertTrue(urls.contains(url));
		
		String key = "";
		
		homeController.redirect(key, this.httpRequest, this.httpResponse);
		Assert.assertTrue(this.httpResponse.getStatus() == HttpStatus.NOT_FOUND.value());
		
		key = "3cd28fc4";
		this.httpResponse = new MockHttpServletResponse();
		homeController.redirect(key, new MockHttpServletRequest(),  this.httpResponse );
		Assert.assertTrue(this.httpResponse.getStatus() == HttpStatus.FOUND.value() 
				|| this.httpResponse.getStatus() == HttpStatus.TEMPORARY_REDIRECT.value());
		
	}
}
	